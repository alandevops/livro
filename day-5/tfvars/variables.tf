variable "ec2_region" {
    default = "us-east-2"
}

variable "ec2_key_name" {
    default = "aws-class"
}

variable "ec2_instance_type" {
    default = "t2.micro"
}

variable "ec2_image_id" {
    default = "ami-00399ec92321828f5"
}

variable "ec2_tags" {
    default = "descomplicando o Gitlab"
}

variable "ec2_instance_count" {
    default = "2"  
}